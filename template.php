<?php
function internet_services_rtl_primary($items = array()) {
  $url = 'http';
  if ($_SERVER['HTTPS']=='on')
    $url .=  's';
  $url .=  '://';
  if ($_SERVER['SERVER_PORT']!='80')
    $url .=  $_SERVER['HTTP_HOST'].':'.$_SERVER['SERVER_PORT'].base_path();
  else
    $url .=  $_SERVER['HTTP_HOST'].base_path();
  if ($_SERVER['QUERY_STRING']>' ')
    $url .=  '?'.$_SERVER['QUERY_STRING'];

  $output = '<ul>';
  if (!empty($items)) {
    foreach ($items as $item) {
      preg_match("/<a\s*.*?href\s*=\s*['\"]([^\"'>]*).*?>(.*?)<\/a>/i", $item, $matches);
      if ($url == $matches[1]) {
        $output .= '<li class="active">'. $item .'</li>';
      } else {
        $output .= '<li>'. $item .'</li>';
      }
    }
  }
  $output .= '</ul>';
  return $output;
}


function internet_services_rtl_regions() {
  return array(
    'right' => t('right sidebar'),
    'content' => t('content'),
    'header' => t('header'),
    'footer' => t('footer'),
    'hidden' => t('hidden'),
  );
}

function phptemplate_stylesheet_import($stylesheet, $media = 'all') {
  $rtl = in_array(locale_initialize(), array('ar', 'fa', 'he', 'ur'));
  if (!$rtl) {
    return theme_stylesheet_import($stylesheet, $media);
  }
  if ($stylesheet == base_path() . 'misc/drupal.css') {
    $stylesheet = base_path() . 'misc/drupal-rtl.css';
  }
  if ($stylesheet == base_path() . path_to_theme() . '/style.css') {
    $stylesheet = base_path() . path_to_theme() . '/style-rtl.css';
  }
  return theme_stylesheet_import($stylesheet, $media);
}
?>
