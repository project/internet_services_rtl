<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>  
</head>

<body<?php print $onload_attributes ?>>
<div id="content">
  <div id="header">
    <?php if (module_exist("adsense")) { ?>
      <div id="adsense"><?php print adsense_display("468x60"); ?></div>
    <?php } ?>
    <?php if ($logo) { ?>
      <div id="logo"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div>
    <?php } ?>
    <?php if ($site_name) { ?> 
      <h1><a class="title" href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1>
    <?php } ?>
    <?php if ($site_slogan) { ?> 
      <div id="slogan"><?php print $site_slogan ?></div>
    <?php } ?>
    <?php if ($header) { ?>
      <div id="block-header"><?php print $header ?></div>
    <?php } ?>
  </div>  <!--end header-->
  <?php if ($primary_links) { ?>
    <div id="bar">      
      <?php print theme('primary', $primary_links) ?> 
    </div>  <!--end bar-->
  <?php } ?>
  <div id="search_bar">
    <?php global $user; ?>  
    <?php if (!$user->uid) { ?>
      <?php print t('<p>Please %login or %register<br />%password</p>', array('%login' => l(t('Log in'), "user"), '%register' => l(t('Register'), "user/register"), '%password' => l(t('Request New Password'), "user/password"))) ?>
    <?php } else { ?>
      <?php print t('<p>Welcome %user<br />%view | %edit | %logout</p>', array('%user' => $user->name, '%view' => l(t('View'), "user/{$user->uid}"), '%edit' => l(t('Edit'), "user/{$user->uid}/edit"), '%logout' => l(t('Logout'), "logout"))) ?>
    <?php } ?>
    <div id="search_box">
      <p><?php print t('Search the Site:') ?> </p>
      <?php print $search_box ?>
      <?php print l(t('Advanced'), "search") ?>
    </div>
  </div>  <!--end search_field-->
  <div id="left">
    <?php if ($mission) { ?> 
      <div id="mission"><?php print $mission ?></div> 
    <?php } ?>
    <div id="path"><?php print $breadcrumb ?></div>
    <div class="tabs"><?php print $tabs ?></div> 
    <div class="pagetitle"><h2><?php print $title ?></h2></div> 
    <?php print $help ?>
    <?php print $messages ?>
    <?php print $content; ?>
  </div>  <!--end left-->
  <div id="right">
    <?php if ($sidebar_right) { ?> 
      <?php print $sidebar_right ?> 
    <?php } ?>
    <?php if ($sidebar_left) { ?> 
      <?php print $sidebar_left ?> 
    <?php } ?>
  </div>  <!--end right-->
  <div id="footer">
  <?php if ($footer_message) { ?> 
    <?php print $footer_message ?>
  <?php } ?>
  <?php if ($hidden) { ?> 
    <?php print $hidden ?>
  <?php } ?>
  <?php print $closure ?>
  </div>  <!--end footer-->
</div>  <!--end content-->
</body>
</html>
