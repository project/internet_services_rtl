<div class="comment"> 
  <?php if ($picture) {
    print $picture;
  } ?> 
  <span class="commentauthor" style="font-weight: bold;"><?php print $author; ?></span> <small class="commentmetadata"><a href="#comment-<?php print $comment->cid; ?>"><?php print $date; ?></a></small> 
  <div class="itemtext"> <?php print $content; ?> </div> 
  <div class="links">&raquo; <?php print $links; ?></div> 
</div>
