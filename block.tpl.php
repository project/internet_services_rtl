<div class="block block-<?php print $block->module; ?> block-<?php print $block->region; ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>"> 
  <div class="blockhead blockhead-<?php print $block->region; ?>"><?php print $block->subject; ?></div>
  <div class="blockbody blockbody-<?php print $block->region; ?>"><?php print $block->content; ?></div> 
</div>
