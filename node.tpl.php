<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($picture) {
      print $picture;
    }?> 
  <div class="itemhead"> 
    <?php if ($page == 0) { ?> 
      <h2 class="title">
        <a href="<?php print $node_url?>" rel="bookmark" title="<?php print t('Permanent Link to') ?> &quot;<?php print $title?>&quot;"><?php print $title?></a>
      </h2>
    <?php }; ?>
		<?php if ( node_access('update', $node) ) { ?>
      <span class="editlink">
      <?php
        $img_link = '<img src="'. base_path().$directory .'/images/pencil.png" alt="'. t('Edit') .'" />';
        $attributes = array('title' => t('Edit'));
        print l($img_link, "node/{$node->nid}/edit", $attributes, NULL, NULL, FALSE, TRUE);
      ?>
      </span>
    <?php }; ?>
    <small class="metadata">
      <span class="chronodata"><?php print t('Published') ?> <?php print $date ?></span>
      <?php if ($terms) { ?> 
        <span class="tagdata"><?php print t('Tags:') ?> <?php print $terms?></span> 
      <?php } ?>
      <?php if ($links) { ?>
        <span class="commentslink"><?php print $links?></span> 
      <?php } ?>
    </small>
  </div>
  <div class="itemtext">
    <?php print $content?> 
  </div> 
</div>
